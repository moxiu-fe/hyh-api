var http = require('http');
var express = require('express');
var path = require('path');
var mongoose = require('mongoose');
var log4js = require('log4js');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var expressValidator = require('express-validator');

var config=require('./config/config')

var system = require('./controllers/system');
var user = require('./controllers/user');
var section = require('./controllers/section');
var topic = require('./controllers/topic');


var app = express();

//连接mongodb
mongoose.connect(config.db);
var db = mongoose.connection;
db.on('error', function () {
  throw new Error('unable to connect to database at ' + config.db)
})

//配置log4
log4js.configure({
  appenders: [
    {
      type: 'console',
      category: "console"
    }, //控制台输出
    {
      type: "file",
      filename: './logs/hyh-api-log.log',
      pattern: "yyyy-MM-dd",  //文件日期格式
      maxLogSize: 2048,
      backups: 4,
      category: 'log'
    }
  ],
  levels:{
    "console":"ALL",
    "log":"ERROR"
  },
  replaceConsole: true   //替换console.log
});


if(app.get('env') === 'production'){
  app.use(log4js.connectLogger(log4js.getLogger('log')))  //生产环境日志输出到日志文件
}else{
  app.use(log4js.connectLogger(log4js.getLogger('console')))  //开发环境日志输出到控制台
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator());  //验证请求参数（body,params,cookie)是否合法中间件
app.use(cookieParser());

app.use('/',system);
app.use('/user',user);
app.use('/section',section);
app.use('/topic',topic);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.json('error');
});


//开启服务
var port = normalizePort(process.env.PORT || config.port);

app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

var logger = require('log4js').getLogger('console')  //日志输出到控制台

server.listen(port,function(){
    logger.info('Express server listening on port ' + port);
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            logger.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    logger.info('Listening on ' + bind);
}
