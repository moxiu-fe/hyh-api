/**
 * Created by cgy on 2016/7/15.
 */
var api = require("./api.js")
var rest = require("./apisender.js")

/**
 * initialize parse with secrets (your appId and restAPIKey)
 *
 * @param appId:  Application Id from Parse
 * @param restAPIKey:  REST API Key from Parse
 */
/*exports.initialize = function(appId, restAPIKey)
 {
 _appId = appId
 _restAPIKey = restAPIKey
 }*/


/**
 * Convenience function to create http options for parse.
 * This sets up the url, port and headers while expecting callers to set path and method
 */
var getParseOptions = function () {
  var options = {
    host: 'api.weixin.qq.com',
    port: 443,
    path: '',
    method: 'GET',
    headers: {
      'Content-Type': 'application/json'
    }
  }

  return options
}


/**
 * wx_oauth_step_1
 * @param className
 * @param onResults: callback function for results
 */
exports.wx_oauth_step_1 = function (className, onResults) {
  console.log("wx_oauth_step_1")

  var options = getParseOptions()
  options.path = api.URL_WX_OAUTH_1

  console.log(options)
  rest.getJSON(options, onResults)
}


/**
 * wx_oauth_step_2
 * @param className
 * @param itemId to retrieve
 * @param onResults: callback function for results
 */
exports.wx_oauth_step_2 = (code, onResults) => {
  console.log("------------- wx_oauth_step_2")
  var options = getParseOptions()
  options.path = api.URL_WX_OAUTH_2.replace(/CODE/, code)
  console.error(options)
  rest.getJSON(options, onResults)
}


/**
 * wx_oauth_step_3
 * @param className
 * @param item to create
 * @param onResults: callback function for results
 */
exports.wx_oauth_step_3 = (access_token, onResult) => {
  console.log("------------- wx_oauth_step_3")
  var options = getParseOptions()
  options.method = 'GET'
  options.path = api.URL_WX_OAUTH_3.replace(/ACCESS_TOKEN/, access_token)
  rest.getJSON(options, onResult)
}

