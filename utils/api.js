var Config = require('../../config/config')

exports.URL_WX_OAUTH_1='https://open.weixin.qq.com/connect/oauth2/authorize?appid='+ Config.WX_APPID +'&redirect_uri=http%3A%2F%2Fci.seatell.cn%2Foauth_response.html&response_type=code&scope=snsapi_userinfo&state=STATE#wechat_redirect'

exports.URL_WX_OAUTH_2='https://api.weixin.qq.com/sns/oauth2/access_token?appid=' + Config.WX_APPID + '&secret=' + Config.WX_APPSECRET + '&code=CODE&grant_type=authorization_code'

exports.URL_WX_OAUTH_3='https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID&lang=zh_CN'




