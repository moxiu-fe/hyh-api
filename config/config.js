/**
 * Created by luojian on 17-5-16.
 */
var env = process.env.NODE_ENV || 'production';

var config = {
    development: {
        host: 'http://localhost',
        port: 4000,
        db: 'mongodb://hyh:123456@123.206.181.73:27017/hyh'
    },
    production: {
        host: 'http://localhost',
        port: 4000,
        // db: 'mongodb://hyh:123456@123.206.181.73:27017/hyh'
        db: 'mongodb://hyh:123456@localhost:27017/hyh'
    }
}

module.exports = config[env];

