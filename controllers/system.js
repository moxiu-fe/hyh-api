var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');

var userSchema = require('../models/User');
var topicSchema = require('../models/Topic')
var appInfoSchema = require('../models/AppInfo')
var speTopicSchema = require('../models/SpeTopic')
var resErrType = require('../constants/resErrType')
var resResult = require('../utils/resResult')
var conVariable = require('../constants/conVariable')


var router = express.Router();
var Users = mongoose.model('User', userSchema);
var Topics = mongoose.model('Topic', topicSchema);
var AppInfo = mongoose.model('AppInfo', appInfoSchema);
var SpeTopics = mongoose.model('SpeTopic', speTopicSchema);


router.post('/signup', function (req, res, next) {
    //验证请求body
    req.checkBody({
        'phone': {
            notEmpty: true,
            isNumeric: true,
            isMobilePhone: {
                options: ['zh-CN'],
                errorMessage: 'Invalid phone'
            },
            errorMessage: 'Invalid phone'
        },
        'password': {
            notEmpty: true,
            errorMessage: 'Invalid Password'
        },
        'confirmPassword': {
            notEmpty: true,
            errorMessage: '两次输入的密码不相等'
        },
    });
    var errors = req.validationErrors()
    if (errors) return res.json(resResult.error(resErrType.ERR_INVALID_BODY, errors))

    if (req.body.password !== req.body.confirmPassword) return res.json(resResult.error({
        code: 212,
        desc: '两次输入的密码不相等'
    }));

    next()
}, function (req, res, next) {
    var phone = req.body.phone, password = req.body.password;

    //判断用户是否已注册
    Users.findOne({phone: phone}, function (err, user) {
        if (err) return next(err)
        if (user) return res.json(resResult.error({code: 222, desc: '已注册，请直接登录'})); //已注册

        bcrypt.genSalt(conVariable.BCRYPT_SALT_ROUNDS, function (err, salt) {  //未注册,hash密码并存储
            if (err) return next(err)

            bcrypt.hash(password, salt, function (err, hash) {
                if (err) return next(err)

                var user = new Users({
                    phone: phone,
                    password: hash,
                    nickname: '',
                    avatar: '',
                    desc: '',
                    registerFrom: 'phone',
                    registerTime: new Date()
                })
                user.save(function (err, user) {
                    if (err) return next(err)

                    jwt.sign({uid: user._id}, conVariable.JWT_TOKEN_SECRET, {expiresIn: conVariable.JWT_VALID_DAYS + ' days'}, function (err, token) {
                        if (err) return next(err)
                        return res.json(resResult.success({token: token}))  //注册成功，返回token
                    });
                })
            });

        });
    })

});

router.post('/login', function (req, res, next) {
    next()
}, function (req, res, next) {

    var phone = req.body.phone, password = req.body.password;

    Users.findOne({phone: phone}, function (err, user) {
        if (err) return next(err)
        if (!user) return res.json(resResult.error({code: 222, desc: '未注册，请先注册'}))

        bcrypt.compare(password, user.password, function (err, equal) {
            if (err) return next(err)

            if (!equal) return res.json(resResult.error({code: 221, desc: '密码错误，请重新登录'}))

            jwt.sign({uid: user._id}, conVariable.JWT_TOKEN_SECRET, {expiresIn: conVariable.JWT_VALID_DAYS + ' days'}, function (err, token) {
                if (err) return next(err)
                return res.json(resResult.success({token: token}))  //密码正确，登录成功，返回token
            });
        })

    })

});

router.post('/appInfo/add', function (req, res, next) {
    next()
}, function (req, res, next) {

    var appId = req.body.appId,
        appName = req.body.appName,
        version = req.body.version,
        build = req.body.build,
        updateInfo = req.body.updateInfo,
        appUrl = req.body.appUrl,
        platform = req.body.platform;

    var appInfo = new AppInfo({
        appId: appId,
        appName: appName,
        platform: platform,
        version: version,
        build: build,
        updateInfo: updateInfo,
        appUrl: appUrl
    })
    appInfo.save(function (err, result) {
        if (err) return next(err)
        var appInfo = result.toObject()
        delete appInfo.__v;
        delete appInfo._id;
        return res.json(resResult.success(appInfo))
    })


});

router.post('/appInfo/get', function (req, res, next) {
    next()
}, function (req, res, next) {

    var appId = req.body.appId, platform = req.body.platform;

    AppInfo.findOne({
        appId: appId,
        platform: platform
    }, "-_id appId appName platform version build updateInfo appUrl", function (err, appInfo) {
        if (err) return next(err)


        if (appInfo) return res.json(resResult.success(appInfo))
    })

});




router.post('/banner/add', function (req, res, next) {
    next()
}, function (req, res, next) {
    var topicId = req.body.topicId;

    Topics.findById(topicId, function (err, topic) {
        if (err) return next(err)
        if (!topic) res.json(resResult.error(resErrType.ERR_INVALID_BODY, {desc: '无效的topicId'}))

        SpeTopics.findOneAndUpdate(
            {name: 'banner'},
            {
                $addToSet: {topics: topic._id},
                $inc: {'topicTotal': 1}
            },
            {new: true, upsert: true},
            function (err, banneTopic) {
                if (err) return next(err)
                return res.json(resResult.success(banneTopic))
            })
    })
});

router.get('/banner/get', function (req, res, next) {
    next()
}, function (req, res, next) {

    SpeTopics.find({name: 'banner'})
        .populate({
            path: 'topics',
            select: '_id title imgUrls',
            options: {sort: {lastUpdatedTime: -1}, limit: 5}  //时间降序
        })
        .select({_id: 0, topics: 1})
        .exec(function (err, result) {
            if (err) return next(err)
            if(!result) return res.json(resResult.success([]))

            var banner = result[0].toObject()
            return res.json(resResult.success(banner))
        })
});




router.post('/essence/add', function (req, res, next) {
    next()
}, function (req, res, next) {
    var topicId = req.body.topicId;

    Topics.findById(topicId, function (err, topic) {
        if (err) return next(err)
        if (!topic) res.json(resResult.error(resErrType.ERR_INVALID_BODY, {desc: '无效的topicId'}))

        SpeTopics.findOneAndUpdate(
            {name: 'essence'},
            {
                $addToSet: {topics: topic._id},
                $inc: {'topicTotal': 1}
            },
            {new: true, upsert: true},
            function (err, banneTopic) {
                if (err) return next(err)
                return res.json(resResult.success(banneTopic))
            })
    })
});

router.get('/essence/get', function (req, res, next) {
    next()
}, function (req, res, next) {

    SpeTopics.find({name: 'essence'})
        .populate({
            path: 'topics',
            select: '_id title imgUrls auth lastUpdatedTime',
            options: {sort: {lastUpdatedTime: -1}, limit: 5}  //时间降序
        })
        .select({_id: 0, topics: 1})
        .exec(function (err, result) {
            if (err) return next(err)

            var banner = result[0].toObject()
            return res.json(resResult.success(banner))
        })
});


router.post('/search', function (req, res, next) {
    next()
}, function (req, res, next) {
    var tag = req.body.tag, pageNum = parseInt(req.body.pageNum)

    Topics.find({tag: tag}).count(function (err, count) {
        if (err) return next(err);
        var topicTotal = count;
        Topics.find({tag: tag})
            .select({_id: 1, auth: 1, title: 1, content: 1, imgUrls: 1, tag: 1, aggreTotal: 1, lastUpdatedTime: 1})
            .sort({lastUpdatedTime: -1})
            .skip(pageNum * 10-10)
            .limit(10)
            .exec(function (err, topics) {
                if (err) return next(err)

                var result = {};
                result.pageData = topics;
                result.currentPage = pageNum;
                result.totalPage = Math.ceil(topicTotal / 10);
                return res.json(resResult.success(result))
            })
    })


});


module.exports = router;
