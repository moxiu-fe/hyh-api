var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');

var sectionSchema = require('../models/Section')
var resErrType = require('../constants/resErrType')
var resResult = require('../utils/resResult')
var conVariable = require('../constants/conVariable')

var router = express.Router();
var Sections = mongoose.model('Section', sectionSchema);

router.post('/add', function (req, res, next) {
  var name = req.body.name, iconUrl = req.body.iconUrl, desc = req.body.desc;
  var section = new Sections({
    name: name,
    iconUrl: iconUrl,
    desc: desc
  })

  section.save(function (err, section) {
    if (err) return next(err);

    return res.json(resResult.success(section))
  })
});

router.get('/list', function (req, res, next) {
  Sections.find({}).select({_id: 1, name: 1, iconUrl: 1, desc: 1}).sort({lastUpdatedTime:-1}).exec(function (err, sections) {
    if (err) return next(err);

    return res.json(resResult.success({  //返回所有论坛
      sections: sections
    }))
  })
});



module.exports = router;
