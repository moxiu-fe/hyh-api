var express = require('express');
var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var log4js = require('log4js');

var tokenVerify = require('./cusMiddleWares/tokenVerify')
var topicSchema = require('../models/Topic')
var sectionSchema = require('../models/Section')
var userSchema = require('../models/User')
var resErrType = require('../constants/resErrType')
var resResult = require('../utils/resResult')
var conVariable = require('../constants/conVariable')

var router = express.Router();
var Sections = mongoose.model('Section', sectionSchema);
var Topics = mongoose.model('Topic', topicSchema);
var Users = mongoose.model('User', userSchema);
var logger = log4js.getLogger('console');

router.post('/list', function (req, res, next) {
    next()
}, function (req, res, next) {
  var sid = req.body.sid,pageNum=parseInt(req.body.pageNum);

  Sections.findById(sid)
      .populate({
        path:'topics',
        select:'_id auth title content imgUrls tag aggreTotal lastUpdatedTime',
        options:{sort:{lastUpdatedTime:-1},skip:pageNum*10-10,limit:10}  //时间降序
      })
      .select({_id: 0, topics: 1, topicTotal: 1})
      .exec(function (err, result) {
        if (err) return next(err)

        var topics=result.toObject();
        if(topics.topics.length===0) return res.json(resResult.error(resErrType.ERR_INVALID_BODY,'无效的pageNum'))

        topics.currentPage=pageNum;
        topics.totalPage=Math.ceil(topics.topicTotal/10);
        return res.json(resResult.success(topics))
      })

});

router.post('/detail', function (req, res, next) {
    var tid = req.body.tid;

    Topics.findById(tid)
        .populate('comments.atAuth')
        .select({
            _id: 1,
            auth: 1,
            title: 1,
            content: 1,
            imgUrls: 1,
            tag: 1,
            section: 1,
            aggreTotal: 1,
            comments: 1,
            lastUpdatedTime: 1
        })
        .exec(function (err, result) {
            if (err) return next(err)

            return res.json(resResult.success(result))
        })

});

//发帖
router.post('/add', tokenVerify, function (req, res, next) {
    var user = req.body.user,
        sid = req.body.sid,
        title = req.body.title,
        content = req.body.content,
        imgUrls = req.body.imgUrls,
        tag = req.body.tag;

    var topic = new Topics({
        auth: user,
        section: sid,
        title: title,
        content: content,
        imgUrls: imgUrls,
        tag: tag
    })
    topic.save(function (err, topic) {
        if (err) return next(err)

        Sections.findOneAndUpdate(
            {_id: sid},
            {
                $addToSet: {topics: topic._id},
                $inc: {'topicTotal': 1}
            },
            {new: true, upsert: false},
            function (err, section) {
                if (err) return next(err)

                Users.findOneAndUpdate(
                    {_id: user._id},
                    {
                        $addToSet: {sendTopics: topic._id},
                        $inc: {sendTopicTotal: 1}
                    },
                    {new: true, upsert: false},
                    function (err, user) {
                        if (err) return next(err)

                        return res.json(resResult.success({
                            _id: topic._id
                        }))
                    }
                )
            }
        )
    })
});

//帖子点赞
router.post('/aggre', tokenVerify, function (req, res, next) {
    var user = req.body.user,
        tid = req.body.tid;

    Topics.findOneAndUpdate(
        {_id: tid},
        {
            $inc: {'aggreTotal': 1}
        },
        {new: true, upsert: false, select: '-_id aggreTotal'}
    )
        .exec(function (err, aggreTotal) {
                if (err) return next(err)
                if(!aggreTotal) return res.json(resResult.error(resErrType.ERR_INVALID_BODY))

                return res.json(resResult.success(aggreTotal))
            }
        )
});

//评论
router.post('/reply', tokenVerify, function (req, res, next) {
    var user = req.body.user,
        tid = req.body.tid,
        content = req.body.content,
        imgUrls = req.body.imgUrls,
        atAuth = req.body.atAuth;

    Users.findById(atAuth, "_id nickname avatar", function (err, atUser) {
        if (err) return next(err)
        if (!atUser) return res.json(resResult.error(resErrType.ERR_INVALID_BODY, {desc: '无效的atAuth'}))

        Topics.findOneAndUpdate(
            {_id: tid},
            {
                $addToSet: {
                    comments: {
                        auth: user,
                        content: content,
                        imgUrls: imgUrls,
                        atAuth: atUser
                    }
                },
                $inc: {'commentTotal': 1}
            },
            {new: true, upsert: false, select: '_id comments'}
        )
            .exec(function (err, topic) {
                    if (err) return next(err)

                    Users.find({_id: user._id, sendTopics: {$all: [topic._id]}}) //判断用户评论的帖子是否是自己发表的帖子
                        .exec(function (err, user) {
                            if (err) return next(err)
                            var result = topic.toObject({minimize: true})
                            if (user) return res.json(resResult.success())  //是，不添加到用户参与的帖子

                            Users.findOneAndUpdate(   //否，添加到用户参与的帖子
                                {_id: user._id},
                                {
                                    $addToSet: {partTopics: topic._id},
                                    $inc: {partTopicTotal: 1}
                                },
                                {new: true, upsert: false},
                                function (err, user) {
                                    if (err) return next(err)

                                    return res.json(resResult.success())
                                }
                            )
                        })

                }
            )
    })
});

//评论点赞
router.post('/replyAggre', tokenVerify, function (req, res, next) {
    var user = req.body.user,
        tid = req.body.tid,
        rid = req.body.rid;

    Topics.findOneAndUpdate(
        {_id: tid, comments: {$elemMatch: {_id: rid}}},
        {
            $inc: {"comments.$.aggreTotal": 1}
        },
        function (err, result) {
            if (err) return next(err)

            Topics.findOne({
                    _id: tid,
                    comments: {$elemMatch: {_id: rid}}
                }, "comments.$.aggreTotal", function (err, topic) {
                    if (err) return next(err)

                    return res.json(resResult.success({aggreTotal: topic.comments[0].aggreTotal}))
                }
            )
        }
    )
});


module.exports = router;
