var express = require('express');
var mongoose = require('mongoose')

var tokenVerify = require('./cusMiddleWares/tokenVerify')
var userSchema = require('../models/User')
var resErrType = require('../constants/resErrType')
var resResult = require('../utils/resResult')
var conVariable = require('../constants/conVariable')

var router = express.Router();
var Users = mongoose.model('User', userSchema);


router.post('/profile', tokenVerify, function (req, res, next) {
    //验证是否有多余的请求body

    next()
}, function (req, res, next) {
    var user = req.body.user

    Users.findById(user._id, 'nickname avatar desc gender', function (err, user) {
        if (err) return next(err)

        return res.json(
            resResult.success(user)
        )
    })
});

router.post('/modifyProfile', tokenVerify, function (req, res, next) {
    next()
}, function (req, res, next) {
    var uid = req.body.user._id, nickname = req.body.nickname, avatar = req.body.avatar, desc = req.body.desc,
        gender = req.body.gender;

    var updateDoc = {nickname: nickname, avatar: avatar, desc: desc, gender: gender};

    Users.findOneAndUpdate(
        {_id: uid},
        {
            $set:{nickname: nickname, avatar: avatar, desc: desc, gender: gender}
        },
        {new: true, upsert: false, select: 'nickname avatar desc gender'},
        function (err, user) {
            if (err) return next(err)

            return res.json(
                resResult.success({
                    nickname: user.nickname,
                    avatar: user.avatar,
                    desc: user.desc,
                    gender: user.gender
                })
            )
        })

});

//查看用户发表的帖子
router.post('/sendTopics', function (req, res, next) {
    //验证是否有多余的请求body

    next()
}, function (req, res, next) {
    var uid = req.body.uid, pageNum = parseInt(req.body.pageNum);

    Users.findById(uid)
        .populate({
            path: 'sendTopics',
            select: '_id auth title content imgUrls tag aggreTotal lastUpdatedTime',
            options: {skip: pageNum * 10 - 10, limit: 10, sort: {lastUpdatedTime: -1}}
        })
        .select('-_id sendTopicTotal sendTopics')
        .exec(function (err, topics) {
            if (err) return next(err)
            if (!topics) return res.json(resResult.error(resErrType.ERR_INVALID_BODY))

            return res.json(resResult.success({
                currentPage: pageNum,
                totalPage: Math.ceil(topics.sendTopicTotal / 10),
                topics: topics.sendTopics
            }))
        })
});

//查看用户参与的帖子
router.post('/partTopics', function (req, res, next) {
    //验证是否有多余的请求body

    next()
}, function (req, res, next) {
    var uid = req.body.uid, pageNum = parseInt(req.body.pageNum);

    Users.findById(uid)
        .populate({
            path: 'partTopics',
            select: '_id auth title content imgUrls tag aggreTotal lastUpdatedTime',
            options: {skip: pageNum * 10 - 10, limit: 10, sort: {lastUpdatedTime: -1}}
        })
        .select('-_id partTopicTotal partTopics')
        .exec(function (err, topics) {
            if (err) return next(err)
            if (!topics) return res.json(resResult.error(resErrType.ERR_INVALID_BODY))

            return res.json(resResult.success({
                currentPage: pageNum,
                totalPage: Math.ceil(topics.sendTopicTotal / 10),
                topics: topics.sendTopics
            }))
        })
});

router.post('/otherProfile', function (req, res, next) {
    //验证是否有多余的请求body
    next()
}, function (req, res, next) {
    var uid = req.body.uid

    Users.findById(uid, '_id nickname avatar desc gender', function (err, user) {
        if (err) return next(err)

        return res.json(
            resResult.success(user)
        )
    })
});

module.exports = router;
