/**
 * Created by luojian on 17-5-17.
 */
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var validator = require('validator');

var conVariable = require('../../constants/conVariable');
var resErrType = require('../../constants/resErrType');
var resResult = require('../../utils/resResult');
var userSchema = require('../../models/User')
var Users = mongoose.model('User', userSchema);

/*
 * 验证token
 * */
var tokenVerify = function (req, res, next) {
    req.checkBody('token', 'token不能为空').notEmpty();
    var errors = req.validationErrors()
    if (errors) return res.json(resResult.error(resErrType.ERR_INVALID_BODY, errors))

    // invalid token
    jwt.verify(req.body.token, conVariable.JWT_TOKEN_SECRET, function (err, result) {
        if (err || !validator.isMongoId(result.uid)) return res.json(resResult.error(resErrType.ERR_INVALID_TOKEN))

        Users.findById(result.uid, '_id nickname avatar', function (err, user) {
            if (err) return next(err)
            req.body.user = user
            delete req.body.token
            next()
        })
    });

}

module.exports = tokenVerify